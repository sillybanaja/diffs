#### patches for various programs:
## dwm-ontop
When you press MODKEY+Shift+t (or whatever you set it in your config.def.h) you set
a window to be ontop of all windows. This is useful for watching movies ontop of
doing other things or using it with picture in picture videos.

This patch isnt made in mind for Multimonitor support, but it wouldnt be that
difficult to add (few lines), send a issue or a pull.

This is different to the official dwm patch alwaysontop, ontop lets you set
multiple windows to the top, where alwaysontop does not allow. So you can have a
picture in picture video playing Subway Surfers and another playing Minecraft parkour.

![ontop-showcase](https://i.imgur.com/6DChsZB.png)

## dwm-optimalgaps
The best gaps out there.

![optimalgaps-showcase](https://i.imgur.com/974KrH2.png)
